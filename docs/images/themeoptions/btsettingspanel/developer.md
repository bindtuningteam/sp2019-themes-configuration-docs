The **Developer tab** allows for the introduction of minimal custom code modifications, that range from CSS to JavaScript.

![bindtuning-settings-developer.png](../../images/bindtuning-settings-developer.png)

---
#### Custom CSS
This zone, allows you to add your custom CSS to the page

---
#### Custom Script
Allows the addition of custom scripts to the page. 

**Note:** This zone will load after the scripts added in the addicional resources zone.

---
#### Additional Resources
Allows you to add CSS or JS files to your theme. These files load synchronously, in the order you have inserted them. 
