### Platform version
- SharePoint On-Premises 2019 (Modern Experience)

### Browsers

BindTuning Themes work in all modern browsers:

- Firefox
- Chrome
- Safari
- Opera
- Edge
- IE
