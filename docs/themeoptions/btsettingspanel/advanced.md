The **Advanced tab** allows you to manage how and where your site collection settings are stored. 

![bindtuning-settings-advanced.png](../../images/bindtuning-settings-advanced.png)


### Manage

#### Settings source
Allows you to change where your settings are retrieved from: 

- <b>Tenant</b>;
- <b>Site Collection</b>; 
- <b>Other site</b>.

---
### Settings

This option allows you to **import** settings from a specific site or JSON file and **export** to other sites or JSON files.
<p>

This feature proves extremely helpful when wanting to backup your settings and content, or easily propagate them into other site collections. 

![export-btsettings.png](../../images/export-btsettings.png)

---
#### Export Settings

Export to **JSON file**:

1. To export your settings and content open the settings panel and select **Export**; 

2. Under **Apply current settings to** select the option **JSON File**;

3. Click **Execute**.

This will generate a JSON file that you can select when prompted.

---
Export to **Other sites/s**:

1. To export your settings and content open the settings panel and select **Export**; 

2. Under **Apply current settings to** select the option **Other Site/s**;

3. Click on the plus (**+**) icon to add the sites you want the settings to be applied to; 

4. Click **Execute**.

----
#### Import Settings


Import using **JSON file**:

1. To import your settings and content open the settings panel and click **Import**. You will be prompted to select a file. Select the JSON file you’ve exported.

2. Click **Confirm** when the confirmation modal shows up and you’re done!

  ![import-btsettings.png](../../images/import-btsettings.png)
	

--- 
Import using **Hub Site**: 

If your site collection is associated with a Hub Site, you have the flexibility to import its settings. To do this, simply follow the instructions below: 

1. Under **Settings** select **Import**; 
2. Under **Copy settings from** select **Hub site**. 

The settings will be automatically applied, using the configuration provided by your Hub Site. 

---
Import using **Other site**: 

1. Under **Settings** select **Import**; 
2. Under **Copy settings from** select **Other site**; 
3. Paste the URL of the site collection where you want to pull the settings from. 

The settings will be automatically applied, using the configuration provided by the inputed site colelction. 

